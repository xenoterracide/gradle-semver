/*
 * This file was generated by the Gradle 'init' task.
 *
 * This generated file contains a sample Java Library project to get you started.
 * For more details take a look at the Java Libraries chapter in the Gradle
 * user guide available at https://docs.gradle.org/5.1.1/userguide/java_library_plugin.html
 */

plugins {
    idea
    `java-library`
    `java-gradle-plugin`
    id("com.gradle.plugin-publish").version("0.10.0")
}
version = "0.8.4"
group="com.xenoterracide"
repositories {
    mavenCentral()
}

dependencyLocking {
    lockAllConfigurations()
}

dependencies {
    implementation("org.apache.commons:commons-lang3:3.+")
    implementation("org.eclipse.jgit:org.eclipse.jgit:5.+")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

pluginBundle {
    // These settings are set for the whole plugin bundle
    vcsUrl = "https://bitbucket.org/xenoterracide/gradle-plugin-mirror"
    website = vcsUrl
    plugins {
        create("sem-ver") {
            id = "com.xenoterracide.gradle.$name"
            displayName = "Xeno's SemVer with Git"
            description = "Use pure java to parse semantic git tags, and add a project.version"
            tags = listOf("git")
        }
    }
}
